def loadFile(file):
    arq = open(file, 'r')
    content = arq.readlines()
    arq.close()

    #Processar conteúdo
    init = len(content)
    for x in range(len(content)):
        if content[x][0] != '#':
            init = x
            break

    dados = []
    for linha in content[init::]:
        if '\n' in linha:
            linha = linha.replace('\n','')

        if file == 'equipamentos.txt':
            temp = []
            for x in linha.split(','):
                temp.append(x.strip())

            linha = temp[:]
        else:
            linha = linha.split()

        dados.append(linha)

    return dados

def consumo(equip,volts):

    consumoTotal = 0
    
    for dispo in equip:
        qnt = int(dispo[0])

        potencia = dispo[2]
        amperagem = dispo[3]

        if potencia == 'x':
            dispo[2] = str(volts * float(dispo[3]))
            
        elif amperagem == 'x':
            amperagem = float(dispo[2])/float(volts)
            dispo[3] = '%.2f' %amperagem

        consumoTotal += (qnt * float(amperagem))

    return consumoTotal

def selectDisj(consumoTotal, disj):

    for disjunt in disj:
        if int(disjunt[0]) > consumoTotal:
            return disjunt

def selectBitola(disjuntor, bit):

    for bito in bit:
        if int(bito[2]) > int(disjuntor[0]):
            return bito
            
def processarResultado(equip, consumoTotal, disjuntor, bitola):
    arq = open('Resultado.txt', 'w')

    arq.write('Consumo Total do Circuito: %.2fA\n' %consumoTotal)
    arq.write('Disjuntor para o Circuito: %sA\n' %disjuntor[0])
    arq.write('Bitola para o Circuito: %sA\n\n' %bitola[2])

    equip = [['Quantidade','Nome','Potência','Corrente(Amperagem)']]+equip

    maior1 = len(sorted(equip, key= lambda x: len(x[0]))[::-1][0][0])
    maior2 = len(sorted(equip, key= lambda x: len(x[1]))[::-1][0][1])
    maior3 = len(sorted(equip, key= lambda x: len(x[2]))[::-1][0][2])
    maior4 = len(sorted(equip, key= lambda x: len(x[3]))[::-1][0][3])
    
    for x in equip:
        linha = x[0]
        if len(x[0]) < maior1:
            linha += ' '* (maior1-len(x[0]))
        linha += ' |'

        linha += x[1]
        if len(x[1]) < maior2:
            linha += ' '* (maior2-len(x[1]))
        linha += ' |'

        linha += x[2]
        if len(x[2]) < maior3:
            linha += ' '* (maior3-len(x[2]))
        linha += ' |'

        linha += x[3]
        if len(x[3]) < maior4:
            linha += ' '* (maior4-len(x[3]))
        linha += ' |'

        arq.write(linha+'\n')

    arq.close()

#Carregando dados
bit = loadFile('bitolas.txt')
disj = loadFile('disjuntores.txt')
equip = loadFile('equipamentos.txt')

volts = int(input('Digite a voltagem do circuito, ex., 220 ou 127: '))

#Calculando consumo Total do circuito
consumoTotal = consumo(equip,volts)

#Definindo Disjuntor
disjuntor = selectDisj(consumoTotal, disj)

#Definindo Bitola
bitola = selectBitola(disjuntor, bit)

processarResultado(equip, consumoTotal, disjuntor, bitola)
print('Resultado salvo em Resultado.txt')
