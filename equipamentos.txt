#Arquivo para a lista de equipamentos para o circuito
#Modelo: Quantidade, Nome, Potência, Corrente(Amperagem)
#
#Exemplos:
# 1, Televisão LCD, 200, 0.91
# 2, Lâmpadas LED, 10, x
# 1, Barbeador, x, 0.5
#
#Usasse 'x' quando não se sabe o valor
 1, Televisão LCD, 200, 0.91
 2, Lâmpadas LED, 10, x
 1, Barbeador, x, 0.5